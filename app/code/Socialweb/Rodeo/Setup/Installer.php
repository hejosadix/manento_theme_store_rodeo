<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Socialweb\Rodeo\Setup;

use Magento\Framework\Setup;

class Installer implements Setup\SampleData\InstallerInterface {

    /**
     * @var \Magento\CmsSampleData\Model\Page
     */
    private $page;

    /**
     * @var \Magento\CmsSampleData\Model\Block
     */
    private $block;

    /**
     * @param \Socialweb\Rodeo\Model\Page $page
     * @param \Socialweb\Rodeo\Model\Block $block
     */
    public function __construct(
    \Socialweb\Rodeo\Model\Page $page, 
            \Socialweb\Rodeo\Model\Block $block
    ) {
        $this->page = $page;
        $this->block = $block;
    }

    /**
     * {@inheritdoc}
     */
    public function install() {

        //$this->page->install(['Socialweb_Rodeo::fixtures/pages/pages.csv']);
        $this->page->install(
                [

                    'Socialweb_Rodeo::DemoPages/pages.csv',
                ]
        );
        $this->block->install(
                [

                    'Socialweb_Rodeo::DemoBlocks/blocks.csv',
                ]
        );
    }

}
